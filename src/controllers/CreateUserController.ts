import { Request, Response } from "express";
import { UsersService } from "../services/UsersService";

class CreateUserController {
    public static async handle(request: Request, response: Response) {
        const { name, email, password } = request.body;
        const result = await UsersService.CreateUser({name, email, password});

        return response.status(201).json(result);
    }
}

export { CreateUserController };