import { Request, Response } from "express";
import { UsersService } from "../services/UsersService";

class LoginUserController {
    public static async handle(request: Request, response: Response) {
        const { email, password } = request.body;

        const token = await UsersService.AuthUser({ email, password });

        return response.status(200).cookie("session", token).end();
    }
}

export { LoginUserController };