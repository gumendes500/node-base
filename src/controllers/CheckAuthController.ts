import { Request, Response } from "express";
import { Users } from "../repository/users/model/Users";

class CheckAuthController {
    public static async handle(request: Request, response: Response) {
        const { name, email } = request.user as Users;
        return response.json({
            name,
            email
        });
    }
}

export { CheckAuthController };