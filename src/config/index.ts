import path from "path";

export const IsDev = process.env.NODE_EV === "dev";
export const wwwroot = path.join(__dirname, "../../app/dist");
export const JWT_SALT = "c786f2f7f3271b1f0e00a38f1773b33830642529";