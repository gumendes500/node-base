import "express-async-errors";
import express from "express";
import CORS from "cors";
import { apiRouter } from "./routes/api.routes";
import { errorHandler } from "./middleware/errorHadler";
import { webRouter } from "./routes/web.routes";
import cookieParser from "cookie-parser";

const app = express();
app.use(CORS());
app.use(cookieParser());
app.use(express.json());

app.use("/api", apiRouter);
app.use(webRouter);

app.use(errorHandler);

export { app };