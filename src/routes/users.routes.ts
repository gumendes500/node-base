import { Router } from "express";
import { CheckAuthController } from "../controllers/CheckAuthController";
import { CreateUserController } from "../controllers/CreateUserController";
import { LoginUserController } from "../controllers/LoginUserController";
import { authUser } from "../middleware/authUser";

const usersRouter = Router();

usersRouter.post("/", CreateUserController.handle);
usersRouter.post("/login", LoginUserController.handle);
usersRouter.get("/auth", authUser, CheckAuthController.handle);

export { usersRouter };