import express,{ Request, Response, Router } from "express";
import { createProxyMiddleware } from "http-proxy-middleware";
import path from "path";
import { IsDev, wwwroot } from "../config";

const webRouter = Router();

if (IsDev) {
    webRouter.use(createProxyMiddleware({ target: "http://localhost:3000", changeOrigin: true }));    
} else {
    webRouter.use(express.static(wwwroot));
    webRouter.use(function(request: Request, response: Response) {
        return response.sendFile(path.join(wwwroot, "index.html"));
    });
}


export { webRouter };