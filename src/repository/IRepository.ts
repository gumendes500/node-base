interface IRepository<C,U,F,O> {
    Create(data: C): Promise<O>;
    FindById(id: string): Promise<O>;
    FindMany(filter: F): Promise<O[]>;
    Update(id: string, data: U): Promise<O>;
    Delete(id: string): Promise<O>;
}

export { IRepository };