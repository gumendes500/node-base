import Prisma from "@prisma/client";
import { prismaClient } from "../../prisma";
import { IRepository } from "../IRepository";
import { ICreateUserDTO } from "./dto/create-user.dto";
import { IUpdateUserDTO } from "./dto/update-user.dto";
import { Users } from "./model/Users";

interface IFilterUser {
    id?: string;
    name?: string;
    email?: string;
    password?: string;
    created_at?: Date;
    updated_at?: Date;
}

class UsersRepository implements IRepository<ICreateUserDTO, IUpdateUserDTO, IFilterUser, Users > {
    public async Create({ email, name, password }: ICreateUserDTO): Promise<Users> {
        return await prismaClient.users.create({
            data: {
                email,
                name,
                password
            }
        }) as Users;
    }

    public async FindById(id: string): Promise<Users> {
        return await prismaClient.users.findUnique({
            where: { id }
        });
    }

    public async FindMany({ id, name, email, password, created_at, updated_at }: IFilterUser): Promise<Users[]> {
        return await prismaClient.users.findMany({
            where: {
                id,
                name,
                email,
                password,
                created_at,
                updated_at
            }
        }) as Users[];
    }

    public async Update(id: string, { email, name, password }: IUpdateUserDTO): Promise<Users> {
        return await prismaClient.users.update({
            data: {
                email,
                name,
                password
            },
            where: { id }
        });
    }

    public async Delete(id: string): Promise<Users> {
        return await prismaClient.users.delete({
            where: { id }
        });
    }

    public async FindByEmail(email: string): Promise<Users> {
        return await prismaClient.users.findUnique({ 
            where: { email }
        });
    }
    
}

export const usersRepository = new UsersRepository();