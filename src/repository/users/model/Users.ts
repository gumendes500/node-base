import { users } from "@prisma/client";

class Users implements users {
    id: string;
    name: string;
    email: string;
    password: string;
    created_at: Date;
    updated_at: Date;
}

export { Users };