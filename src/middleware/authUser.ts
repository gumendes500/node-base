import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { JWT_SALT } from "../config";
import { WebError } from "../model/erros/WebError";
import { usersRepository } from "../repository/users";

interface IPayload {
    id: string;
}

export async function authUser(request: Request, response: Response, next: NextFunction) {
    const { session } = request.cookies;

    if (!session) {
        throw new WebError("user not authenticated", 401);
    }

    try {
        const { id } = verify(session, JWT_SALT) as IPayload;
        const user = await usersRepository.FindById(id);
        request.user = user;
        
        return next();
    } catch (err) {
        throw new WebError("user not authenticated", 401);
    }
}