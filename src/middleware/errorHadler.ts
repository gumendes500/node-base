import { NextFunction, Request, Response } from "express";
import { WebError } from "../model/erros/WebError";

export function errorHandler(err: Error, request: Request, response: Response, next: NextFunction) {
    if (err instanceof WebError) {
        const { code, message } = err;
        return response.status(code).json({ message })
    }
    return response.status(500).json({
        message: "Internal error"
    });
}