import { WebError } from "../model/erros/WebError";
import { usersRepository } from "../repository/users";
import { sign } from "jsonwebtoken";
import { JWT_SALT } from "../config";
import { Users } from "../repository/users/model/Users";

interface ICreateUser {
    name: string,
    email: string,
    password: string
}

interface IAuthUser {
    email: string;
    password: string;
}

class UsersService {
    public static async CreateUser({ email, name, password }: ICreateUser): Promise<Users> {
        if (!email) {
            throw new WebError("email not valid", 400);
        }
        if(!name) {
            throw new WebError("name not valid", 400);
        }
        if(!password) {
            throw new WebError("email not valid", 400);
        }
        
        const userExists = await usersRepository.FindByEmail(email);
        if (userExists) {
            throw new WebError("email not valid", 400);
        }

        const newUser = await usersRepository.Create({ email, name, password });
        return newUser;
    }

    public static async AuthUser({ email, password }: IAuthUser): Promise<string> {
        const user = await usersRepository.FindByEmail(email);
        if (!user) {
            throw new WebError("User not exists", 400);
        }

        if (user.password !== password) {
            throw new WebError("User not exists", 400);
        }

        const token = sign({
            id: user.id
        }, JWT_SALT);

        return token;
    }
}

export { UsersService };